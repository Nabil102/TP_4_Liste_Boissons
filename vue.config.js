module.exports = {
  pwa: {
    display: 'standalone',
    background_color: '#000000',
    themeColor: '#000000',
    msTileColor: '#000000',
    backgroundColor: '#000000',
    theme_color: '#000000',
    manifestOptions: {
      name: 'Boissons peps',
      short_name: 'Boissons peps',
      icons: [{ src: './img/icons/android-chrome-192x192.png', sizes: '192x192', type: 'image/png', purpose: 'any' },
        { src: './img/icons/android-chrome-512x512.png', sizes: '512x512', type: 'image/png', purpose: 'any' },
        { src: './img/icons/android-chrome-maskable-192x192.png', sizes: '192x192', type: 'image/png', purpose: 'any maskable' },
        { src: './img/icons/android-chrome-maskable-512x512.png', sizes: '512x512', type: 'image/png', purpose: 'any maskable' }]
    }
  }
}
